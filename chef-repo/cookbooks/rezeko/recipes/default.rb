#
# Cookbook Name:: rezeko
# Recipe:: default
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

#bash "install REE in RVM" do
#  user "root"
#  code "rvm install ree --passenger"
#  not_if "rvm list | grep ree"
#end

include_recipe "rvm::ree"

package "wkhtmltopdf" do
  action :install
end

gem_package "bundler" do
  gem_binary "/usr/local/rvm/bin/gem-ree-1.8.7-2012.02"
  only_if { File.exist? "/usr/local/rvm/bin/gem-ree-1.8.7-2012.02" }
end

