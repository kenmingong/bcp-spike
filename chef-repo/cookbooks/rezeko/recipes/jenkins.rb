#
# Cookbook Name:: rezeko
# Recipe:: jenkins-slave
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package "libshadow-ruby1.8" do
  action :install
end 

user "jenkins-slave" do
  comment "Jenkins Slave"
  uid 1003
  gid "sudo"
  home "/home/jenkins-slave"
  shell "/bin/sh"
  password "$1$l87oMfhb$3ddAkoEK4a6KjmazibSFd1"
end

include_recipe "jenkins::node_jnlp"
