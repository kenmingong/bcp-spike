#!/bin/sh
JSON="{ \"run_list\": [ "
while [ $# -gt 1 ]; do
  JSON="$JSON\"$1\", "
  shift
done
if [ $# -ge 1 ]; then
  JSON="$JSON\"$1\" ] } "
else
  JSON="$JSON ] } "
fi
echo $JSON > solo_autogen.json
sudo chef-solo -c solo.rb -j solo_autogen.json
