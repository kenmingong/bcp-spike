name        "rezeko-server"
description "Installs and configures a base RezEko Web Engine Server"
run_list    "recipe[rezeko::default]", "recipe[git]", "recipe[rvm::ree]", "recipe[memcached]", "recipe[redis]"
override_attributes 'redis' => { 'bind' => '0.0.0.0', "demonize" => 'yes' }
