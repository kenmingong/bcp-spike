name        "jenkins-server"
description "Installs and configures the jenkins server"
run_list    "recipe[jenkins]", "recipe[rezeko::jenkins]"
override_attributes 'jenkins' => { 'node' => {  "password" => "$1$l87oMfhb$3ddAkoEK4a6KjmazibSFd1", 
                                                "home" => "/home/jenkins-slave", 
                                                "user" => "jenkins-slave" } }
