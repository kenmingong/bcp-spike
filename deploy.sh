#!/bin/sh
#
# RezEko Deployment Script (2012, Aug 09)
# Usage: $0 [options] [FQDN|IpAddress] 
# 
# options:
#  --                            Stop processing options
#  -r, --run-list RUN_LIST       Include run_list for chef-solo runs
#  -h, --help                    Show this help message
#  -V, --version                 Show version information
#
# FQDN | Ip Address:
#  Fully Qualified Domain Name or IP Address of Target Server
#
#
# Assumption that target servers have openssh-server installed and 
# allows sudo through tty access
# -- sudo apt-get install ssh
# -- /etc/sudoers must contain:
#      %sudo	ALL=(ALL)	NOPASSWD:ALL
#


usage()
{
    sed -n -e '/^# RezEko/,/^$/ s/^# \?//p' < $0;
    exit 1;	
}

version()
{
    echo "RezEko Deployment Script (2012, Aug 09)";
    exit 1;
}

[ $# -eq 0 ] && usage

set -- `getopt -n$0 --longoptions="run-list:,help,version" "r:hV" "$@"` || usage

while [ $# -gt 0 ]
do
  case $1 in
	--version | -V)
	  version;;
    --help | -h)
      usage;;
    --run-list | -r)
      RUN_LIST=`echo $2 | sed -s "s/'//g"`;
      shift;;
    --)
      shift;break;;
    -*)
      usage;;
    *)
      break;;
    esac
    shift
done

if [ -z "$1" ]; then
  echo "You must supply the FQDN or the Ip Address of the node being bootstrapped" >&2
  exit 1
else
  echo "Setting Target Server to $1"
  export HOST=`echo $1 | sed -s "s/'//g"`
fi

run_install()
{
  host=$HOST
  echo "Run List set: $RUN_LIST"
  run_list=$RUN_LIST
  run_list=$(echo $run_list | sed "s/\[/\\\[/g")
  run_list=$(echo $run_list | sed "s/\]/\\\]/g")
  run_list=$(echo $run_list | sed "s/,/\" \"/g")
  run_list=$(echo \"$run_list\")
  echo "sudo bash install.sh $run_list"
  
  # The host key might change when we instantiate a new VM, so
  # we remove (-R) the old host key from known_hosts
  ssh-keygen -R "${host#*@}" 2> /dev/null

  echo "tar cj . | ssh -o 'StrictHostKeyChecking no' \"$host\" '
  sudo rm -rf ~/chef &&
  mkdir ~/chef &&
  cd ~/chef &&
  tar xj &&
  sudo ./install.sh \"$run_list\"'"
  
  tar cj . | ssh -o 'StrictHostKeyChecking no' "$host" '
  sudo rm -rf ~/chef &&
  mkdir ~/chef &&
  cd ~/chef &&
  tar xj &&
  sudo ./install.sh ' "${run_list}"
  
}

run_install