#!/bin/bash

echo "Total arguments: $#"
echo "*: $*"
echo "@: $@"

if [ $# -ge 1 ]; then
  echo "Setting Run_List for Chef Solo run"
else
  echo "No run list specified. Running Chef Solo with empty run_list" >&2
fi

# This runs as root on the server

chef_binary=/usr/local/bin/chef-solo

# Are we on a vanilla system?
if ! test -f "$chef_binary"; then
    export DEBIAN_FRONTEND=noninteractive
    # Upgrade headlessly (this is only safe-ish on vanilla systems)
    apt-get update &&
    apt-get -o Dpkg::Options::="--force-confnew" \
       --force-yes -fuy dist-upgrade &&
       apt-get -y install wget gcc make libxml2 libxslt-dev build-essential zlib1g-dev libssl-dev libreadline-dev 
    # Install Ruby and Chef
    apt-get install -y ruby ruby-dev rubygems make &&
    sudo gem install --no-rdoc --no-ri chef --version 10.12.0
fi

cd chef-repo && ./solo.sh $@